from distutils.core import setup

setup(name='punch',
      version='0.0',
      description='Loading Uintah FVM data into numpy arrays',
      author='Mike Hansen',
      author_email='mike.hansen@chemeng.utah.edu',
      packages=['punch'],
      install_requires=['numpy', 'scipy', 'matplotlib'],
      zip_safe=False)
