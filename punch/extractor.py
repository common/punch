import tkinter as tkinter
from tkinter import filedialog
from tkinter import ttk
import os
import punch.field as pcf
import xml.etree.ElementTree as xml


class ExtractionFrame(ttk.Frame):
    def __init__(self, parent, *args, **kwargs):
        ttk.Frame.__init__(self, parent, *args, **kwargs)
        self.root = parent
        self.root.wm_title('uda extraction')
        self.grid(column=0, row=0, sticky='nsew')

        ttk.Label(self, text='uintah build:').grid(row=0, column=0, sticky='e')
        self.uintah_build_dir_sv = tkinter.StringVar()
        self.uintah_build_dir_entry = ttk.Entry(self, textvariable=self.uintah_build_dir_sv, width=50)
        self.uintah_build_dir_entry.grid(row=0, column=1, columnspan=7, sticky='w')
        self.uintah_set_button = ttk.Button(self, text='set', command=self.write_uintah_build_dir_file)
        self.uintah_set_button.grid(row=0, column=8)
        self.uintah_build_dir_specified = False
        self.check_uintah_build_dir()

        ttk.Label(self, text='uda index:').grid(row=1, column=0, sticky='e')
        self.uda_sv = tkinter.StringVar()
        self.uda_sv.trace('w', lambda name, index, mode, sv=self.uda_sv: self.process_uda(sv))
        self.uda_entry = ttk.Entry(self, width=50, textvariable=self.uda_sv)
        self.uda_entry.grid(row=1, column=1, columnspan=7, sticky='w')
        self.uda_browse_button = ttk.Button(self, text='browse', command=self.browse_for_uda)
        self.uda_browse_button.grid(row=1, column=8)

        self.var_name_listbox = tkinter.Listbox(self, width=30, selectmode=tkinter.EXTENDED)
        self.var_name_listbox.grid(row=2, column=1, rowspan=3, columnspan=4, sticky='w')
        ttk.Label(self, text='*hold the mouse\n'
                             ' or use shift and ctrl\n'
                             ' to select multiple variables').grid(row=2, column=5, sticky='ne')

        self.raw_text_iv = tkinter.IntVar(value=0)
        self.raw_text_check = ttk.Checkbutton(self, text='text', variable=self.raw_text_iv)
        self.raw_text_check.grid(row=5, column=1, sticky='w')

        self.numpy_iv = tkinter.IntVar(value=1)
        self.numpy_check = ttk.Checkbutton(self, text='numpy', variable=self.numpy_iv)
        self.numpy_check.grid(row=6, column=1, sticky='w')

        self.matlab_iv = tkinter.IntVar(value=1)
        self.matlab_check = ttk.Checkbutton(self, text='matlab', variable=self.matlab_iv)
        self.matlab_check.grid(row=7, column=1, sticky='w')

        self.extract_button = ttk.Button(self, text='extract!', command=self.extract)
        self.extract_button.grid(row=8, column=0)
        self.issue_sv = tkinter.StringVar()
        self.issue_label = ttk.Label(self, textvariable=self.issue_sv)
        self.issue_label.grid(row=9, column=0, columnspan=5, sticky='e')

        self.write_matlab_grid_button = ttk.Button(self, text='write grid to matlab', command=self.write_matlab_grid)
        self.write_matlab_grid_button.grid(row=6, column=5, sticky='w')
        self.matlab_grid_path_entry = ttk.Entry(self, width=20)
        self.matlab_grid_path_entry.grid(row=7, column=5, columnspan=5, sticky='w')
        self.matlab_grid_path_entry.insert(tkinter.END, 'grid')
        ttk.Label(self, text='directory:').grid(row=7, column=4, sticky='e')

        for child in self.winfo_children():
            child.grid_configure(padx=0, pady=2)

        self.extract_button.state(["disabled"])
        self.write_matlab_grid_button.state(["disabled"])

    def check_uintah_build_dir(self):
        if os.path.exists('uintah-build-path'):
            ubd_file = open('uintah-build-path', 'r')
            ubd_path = ubd_file.read()
            if not os.path.exists(ubd_path + '/StandAlone/tools/extractors/lineextract'):
                self.uintah_build_dir_sv.set('!!! uintah-build-path file gives invalid path !!!')
                self.uintah_build_dir_specified = False
            else:
                self.uintah_build_dir_sv.set(ubd_path)
                self.uintah_build_dir_specified = True
            ubd_file.close()
        else:
            self.uintah_build_dir_sv.set('!!! cannot find uintah-build-path file !!!')

    def write_uintah_build_dir_file(self):
        ubd_file = open('uintah-build-path', 'w')
        ubd_file.write(self.uintah_build_dir_sv.get())
        ubd_file.close()
        self.check_uintah_build_dir()

    def process_uda(self, sv):
        uda_index_path = sv.get()
        index = xml.parse(uda_index_path).getroot()
        variable_list = []
        for variable_group in index.findall('variables'):
            for variable in variable_group:
                variable_list.append(variable.attrib['name'])

        for variable in variable_list:
            self.var_name_listbox.insert(tkinter.END, variable)

        uda_input_path = uda_index_path[:-10] + '/input.xml'
        inputfile = xml.parse(uda_input_path).getroot()
        boxtext = inputfile.find('Grid').find('Level').find('Box').find('resolution').text
        boxtext = boxtext.replace('[', '')
        boxtext = boxtext.replace(']', '')
        ncells_str = boxtext.split(',')
        self.ncells = [int(ncs) for ncs in ncells_str]

        self.issue_sv.set('Grid size: ' + boxtext)
        self.extract_button.state(["!disabled"])
        self.write_matlab_grid_button.state(["!disabled"])

    def browse_for_uda(self):
        self.uda_sv.set(filedialog.askopenfilename(initialdir='.'))

    def write_matlab_grid(self):
        import punch.grid as pcg
        mesh = pcg.FVGrid(self.ncells)
        mesh.write_matlab(self.matlab_grid_path_entry.get())
        self.issue_sv.set('Grid written to ' + self.matlab_grid_path_entry.get() + '.')

    def extract(self):
        if self.uintah_build_dir_specified and os.path.exists('uintah-build-path'):
            if self.uda_entry.get():
                uda_index_path = self.uda_entry.get()
                uda_folder_path = uda_index_path[:-10]
                items = list(map(int, self.var_name_listbox.curselection()))
                if len(items):
                    for i in items:
                        print(i)
                        varname = self.var_name_listbox.get(i)
                        pcf.extract_from_uintah_archive(uda_folder_path,
                                                        self.ncells,
                                                        varname,
                                                        varname,
                                                        keep_raw_data=self.raw_text_iv.get(),
                                                        write_numpy=self.numpy_iv.get(),
                                                        write_matlab=self.matlab_iv.get(),
                                                        return_field=False,
                                                        verbose=False)
                    self.issue_sv.set('Done.')
                else:
                    self.issue_sv.set('You need to select at least one variable to extract!')
            else:
                self.issue_sv.set('You need to provide the path to a uda index!')
        else:
            self.issue_sv.set('You must set your uintah build directory!')


if __name__ == '__main__':
    root = tkinter.Tk()
    ExtractionFrame(root)
    root.mainloop()
