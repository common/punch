import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as colormaps
import matplotlib.animation as animation
import punch.grid as pcg
import punch.field as pcf

print('\n----------------------------------------------------------------------------')
print('Demo of the `punch` package for loading Uintah-generated data\n'
      'for finite volume methods into numpy arrays for analysis and visualization.')
print('----------------------------------------------------------------------------')

print('\n1. Check out the grid on the xy, xz, and yz faces of a domain, with a cell highlighted')
pcg.demo_grid('xy')
pcg.demo_grid('xz')
pcg.demo_grid('yz')

print('\n2a. Our example FVGrid, m, has 64 pts in the x, 32 in y, and 16 points in the z direction')
print('\n2b. It discretizes a box from [-0.5, 1.] in the x direction, [0., 1.] in y, and [1., 2.] in z')
ncells = [64, 32, 16]
boxdims = [[-0.5, 1.], [0., 1.], [1., 2.]]
m = pcg.FVGrid(ncells,
               boxdims)

input('\nPress Enter to continue...')
print('\n3a. Our example Field, f, is linear in x, a sine wave in y, and decaying in z')
print('3b. To get the 3D arrays of x-, y-, and z-points of cell centers, use m.cell.X, m.cell.Y, m.cell.Z')
print('3c. These are numpy arrays and can be used in expressions such as `sin,` `exp,` and elementwise arithmetic')
f = pcf.Field(2. * m.cell.X + 1.0 + np.sin(2 * np.pi * m.cell.Y) + np.exp(-m.cell.Z))

input('\nPress Enter to continue...')
print('\n4a. Get the value of f at a point in space with f.point(xidx, yidx, zidx)')
print('4b. f(x0, y0, z0) =', f.point(0, 0, 0))

input('\nPress Enter to continue...')
print('\n5a. Obtain the x-locations of cell centers of an FVGrid, m, with m.cell.x')
print('5b. Obtain the data on a line in the x-direction with f.xline(yidx, zidx)')
plt.plot(m.cell.x, f.xline(1, 2))
plt.xlabel('x')
plt.ylabel('f(x, y1, z2)')
plt.show()

print('5c. For the y direction, use m.cell.y and f.yline(xidx, zidx)')
plt.plot(m.cell.y, f.yline(4, 5))
plt.xlabel('y')
plt.ylabel('f(x4, y, z5)')
plt.show()

print('5d. For the z direction, use m.cell.z and f.zline(xidx, yidx)')
plt.plot(m.cell.z, f.zline(2, 4))
plt.xlabel('z')
plt.ylabel('f(x2, y4, z)')
plt.show()

print('\n6a. Obtain the locations of cell centers in the xy plane with m.cell.XYx and m.cell.XYy')
print('6b. Get the field on an xy plane with f.xyplane(zidx)')
plt.contourf(m.cell.XYx, m.cell.XYy, f.xyplane(0))
plt.xlabel('x')
plt.ylabel('y')
plt.title('f(x, y, z0)')
plt.show()

print('\n6c. Obtain the locations of cell centers in the xz plane with m.cell.XZx and m.cell.XZz')
print('6d. Get the field on an xz plane with f.xzplane(yidx)')
plt.contourf(m.cell.XZx, m.cell.XZz, f.xzplane(4))
plt.xlabel('x')
plt.ylabel('z')
plt.title('f(x, y4, z)')
plt.show()

print('\n6e. Obtain the locations of cell centers in the xz plane with m.cell.YZy and m.cell.YZy')
print('6f. Get the field on a yz plane with f.yzplane(xidx)')
plt.contourf(m.cell.YZy, m.cell.YZz, f.yzplane(8))
plt.xlabel('y')
plt.ylabel('z')
plt.title('f(x8, y, z)')
plt.show()

print('\n7: To use x-faces instead of cell centers, replace m.cell... with m.xfce...')
print('   To use y-faces instead of cell centers, replace m.cell... with m.yfce...')
print('   To use z-faces instead of cell centers, replace m.cell... with m.zfce...')
print('   To use corners instead of cell centers, replace m.cell... with m.crnr...')

print('\n7a. To load a Uintah field obtained with the lineextract tool, use the UintahField class.')
print('7b. You need to specify the path to the extracted file')
print('7c. Unless the keyword argument savenumpy=False is provided, numpy binary files '
      'will be generated for rapid future data loading')
print('7d. Loading from numpy binary will be attempted unless the keyword argument fromnumpy=False is provided')
print('7e. punch will try not to use binary data that is out-of-date with current raw data')

file = 'introduction-data'
phi = pcf.UintahField(file)

boxdims = [[-0.5, 0.5], [0.5, 1.], [0., 1.]]
m = pcg.FVGrid(ncells, boxdims)

print('\n7c. After making a grid, we can plot at a particular time with the t() method of the UintahField.')
print('7d. punch.grid provides a method, plot_domain_face_area, that plots a shaded box to represent a domain face')

plt.contourf(m.cell.XYx,
             m.cell.XYy,
             phi.t(7).xyplane(0),
             levels=np.linspace(0., 1., 10),
             cmap=colormaps.jet)
pcg.plot_domain_face_area(boxdims, plt.gca(), 'xy', fillalpha=0., edgecolor='k')
plt.xlabel('x')
plt.ylabel('y')
plt.title('time: t7')
plt.show()

print('\n7e. Animation through time with matplotlib is straightforward:')
input('\nPress Enter to start the movie...')
fig, ax = plt.subplots(1, 1)


def animate(it):
    ax.cla()
    pcg.plot_domain_face_area(boxdims, ax, 'xy', fillalpha=0., edgecolor='k')
    ax.contourf(m.cell.XYx,
                m.cell.XYy,
                phi.t(it).xyplane(0),
                levels=np.linspace(0., 1., 10),
                cmap=colormaps.jet)
    ax.set_xlabel('x')
    ax.set_ylabel('y')


ani = animation.FuncAnimation(fig, animate, phi.times, repeat=True)
plt.show()
