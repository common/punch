import punch.field as pcf
import punch.grid as pcg
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as colormaps
import matplotlib.animation as animation

extract = True
phiname = 'phi'

if extract:
    ncells = [32, 16, 8]
    uda = 'jet-Re1000.uda.000'
    _, phi = pcf.extract_from_uintah_archive(uda, ncells, phiname, write_matlab=True)
    _, u = pcf.extract_from_uintah_archive(uda, ncells, 'u', write_matlab=True)
    _, v = pcf.extract_from_uintah_archive(uda, ncells, 'v', write_matlab=True)
else:
    phi = pcf.UintahField(phiname)

grid = pcg.FVGrid(phi.ncells)
grid.write_matlab('grid')

fig, ax = plt.subplots(1, 1)


def animate(it):
    ax.cla()
    ax.contourf(grid.cell.XYx,
                grid.cell.XYy,
                phi.t(it).xyplane(0),
                cmap=colormaps.jet)
    ax.set_xlabel('x')
    ax.set_ylabel('y')


ani = animation.FuncAnimation(fig, animate, phi.times, repeat=True)
plt.show()
