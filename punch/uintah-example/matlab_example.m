clc; clear all;
addpath('../matlab-helpers/')

%% load the data and the grid
load('u.mat');
load('v.mat');
load('phi.mat');
cell = load('grid/cell-points.mat');

%% phi contourf on the xy plane
zidx = 1;
figure(1);
colormap jet;
for it = 1:21
    clf;
    contourf(cell.XYx,...
             cell.XYy,...
             xyplane(phi{it}, zidx));
    xlabel('x');
    ylabel('y');
    getframe;
end


%% two yline plots
xidx1 = 12;
xidx2 = 24;
zidx = 1;
figure(2);
colormap autumn;
for it = 1:21
    clf;
    subplot(1, 2, 1);
    plot(yline(phi{it}, xidx1, zidx), cell.y, 'k-');
    hold on;
    plot(yline(phi{it}, xidx2, zidx), cell.y, 'k--');
    legend(['x=',num2str(cell.x(xidx1),2)],...
           ['x=',num2str(cell.x(xidx2),2)],...
           'Location', 'Best')
    xlabel('phi');
    ylabel('y');
    ylim([0, 1]);
    grid on;    
    subplot(1, 2, 2);
    contourf(cell.XYx,...
             cell.XYy,...
             xyplane(phi{it}, zidx));
    hold on;
    plot([cell.x(xidx1), cell.x(xidx1)], [0, 1], 'k-', 'LineWidth', 2);
    plot([cell.x(xidx2), cell.x(xidx2)], [0, 1], 'k--', 'LineWidth', 2);
    xlabel('x');
    ylabel('y');
    getframe;
    pause(0.1);
end


%% phi surf on the xz plane
yidx = 7;
figure(3);
colormap jet;
for it = 1:21
    clf;
    surf(cell.XZx,...
         cell.XZz,...
         xzplane(phi{it}, yidx));
    view(173, 24);
    xlabel('x');
    ylabel('z');
    zlabel('phi');
    shading interp;
    getframe;
end

%% phi surf on the yz plane
xidx = 16;
figure(4);
colormap jet;
for it = 1:21
    clf;
    surf(cell.YZy,...
         cell.YZz,...
         yzplane(phi{it}, xidx));
    view(153, 72);
    xlabel('y');
    ylabel('z');
    zlabel('phi');
    shading interp;
    getframe;
end

%% velocity streamslice + phi contourf
figure(5);
colormap winter;
for it = 1:21
    clf;
    contourf(cell.XYx, cell.XYy, xyplane(phi{it}, zidx));
    h = streamslice(cell.XYx,...
                    cell.XYy,...
                    xyplane(u{it}, zidx),...
                    xyplane(v{it}, zidx));
    set(h, 'Color', 'w', 'LineWidth', 2)
    xlabel('x');
    ylabel('y');
    zlabel('z');
    shading interp;
    getframe;
end

%% phi 3d slicing
xslice = [0.3,0.4,0.6,0.8];
yslice = [0.1,0.4];
zslice = [0.1,0.7];
figure(6);
colormap jet;
for it = 1:21
    clf;
    slice(cell.X,...
          cell.Y,...
          cell.Z,...
          phi{it},...
          xslice,...
          yslice,...
          zslice);
    view(153, 72);
    xlabel('x');
    ylabel('y');
    zlabel('z');
    shading interp;
    getframe;
end