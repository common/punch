function m = zline(field, xidx, yidx)
m = squeeze(field(yidx, xidx, :));