function m = xzplane(field, yidx)
m = squeeze(field(yidx, :, :));