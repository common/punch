function m = yzplane(field, xidx)
m = squeeze(field(:, xidx, :));