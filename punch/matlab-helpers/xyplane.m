function m = xyplane(field, zidx)
m = squeeze(field(:, :, zidx));