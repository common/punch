function m = xline(field, yidx, zidx)
m = squeeze(field(yidx, :, zidx));