function m = yline(field, xidx, zidx)
m = squeeze(field(:, xidx, zidx));