import numpy as np


def compare_floats(f1, f2, abstol=1.e-14):
    """
    Compare floats to within some absolute tolerance
    :param f1: the first float (order doesn't matter)
    :param f2: the second float (order doesn't matter)
    :param abstol: the absolute tolerance, such that |f1 - f2| < abstol
    :return: whether or not the floats are within abstol of each other
    """
    return np.abs(f1 - f2) < abstol


def compare_float_arrays(a1, a2, abstol=1.e-14):
    """
    Compare numpy array of floats to within some absolute tolerance
    :param a1: the first array (order doesn't matter)
    :param a2: the second array (order doesn't matter)
    :param abstol: the absolute tolerance, such that |f1 - f2| < abstol for array elements f1, f2
    :return: whether or not all elements with the same indices are within abstol of each other
    """
    return np.all(compare_floats(a1, a2, abstol))


class Test:
    """
    Help write test messages for a collection of tests

    Attributes:
        status (bool): a boolean of whether or not all tests run through this helper have passed or not
        name (str): the name associated with this collection of tests
    """

    def __init__(self, name):
        """
        Make a punch.Test object
        :param name (str): a string identifier for this collection of tests
        """
        self.status = True
        self.name = name

    def __call__(self, bool, message):
        """
        Parentheses operator for a punch.Test object, use to run a single test
        :param bool: the boolean for testing
        :param message: the message associated with this particular test
        """
        if bool:
            print('- pass', message)
        else:
            print('- fail', message)

    def __str__(self):
        """
        Print 'operator' for a punch.Test object, use to print the result of a collection of tests
        :return: pass/fail with the punch.Test name
        """
        if self.status:
            return '- PASS ' + self.name
        else:
            return '- FAIL ' + self.name
