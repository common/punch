import numpy as np
import scipy.io as scio
import os
import punch.testhelper as pct
import matplotlib.pyplot as plt
import matplotlib.patches as patches


class Grid:
    """
    Stores locations of grid points on various lines and faces

    Attributes:
        x (np.array): 1d np.array of x-direction points
        XYx (np.array): 2d np.array of x-direction points for the xy plane
        XZx (np.array): 2d np.array of x-direction points for the xz plane
        X (np.array): 3d np.array of x-direction points
        y (np.array): 1d np.array of y-direction points
        XYy (np.array): 2d np.array of y-direction points for the xy plane
        YZy (np.array): 2d np.array of y-direction points for the yz plane
        Y (np.array): 3d np.array of y-direction points
        z (np.array): 1d np.array of z-direction points
        XZz (np.array): 2d np.array of z-direction points for the xz plane
        YZz (np.array): 2d np.array of z-direction points for the yz plane
        Z (np.array): 3d np.array of z-direction points
    """

    def __init__(self,
                 npts,
                 dims):
        """
        Make a Grid
        :param npts (list (int)*3): number of points in the x-, y-, and z-directions
        :param dims (list (float, float)*3): domain bounds in the x-, y-, and z-directions
        """
        self.x = np.linspace(dims[0][0], dims[0][1], npts[0])
        self.y = np.linspace(dims[1][0], dims[1][1], npts[1])
        self.z = np.linspace(dims[2][0], dims[2][1], npts[2])
        xt, yt, zt = np.meshgrid(self.x, self.y, self.z, indexing='ij')
        self.X = xt
        self.Y = yt
        self.Z = zt
        self.XYx = xt[:, :, 0]
        self.XYy = yt[:, :, 0]
        self.XZx = xt[:, 0, :]
        self.XZz = zt[:, 0, :]
        self.YZy = yt[0, :, :]
        self.YZz = zt[0, :, :]

    def write_matlab(self, filename_no_ext):
        """
        Write the grid to a matlab binary (.mat) file
        :param filename_no_ext (str): the file name without the '.mat' extension
        """
        scio.savemat(filename_no_ext + '.mat', {'x': self.x,
                                                'y': self.y,
                                                'z': self.z,
                                                'X': np.transpose(self.X, (1, 0, 2)),
                                                'Y': np.transpose(self.Y, (1, 0, 2)),
                                                'Z': np.transpose(self.Z, (1, 0, 2)),
                                                'XYx': self.XYx.T,
                                                'XYy': self.XYy.T,
                                                'XZx': self.XZx,
                                                'XZz': self.XZz,
                                                'YZy': self.YZy,
                                                'YZz': self.YZz})


class FVGrid:
    """
    Stores locations of cell centers, directional faces, and cell corners, along with grid spacings

    Attributes:
        crnr (punch.grid.Grid): a grid of corner points
        cell (punch.grid.Grid): a grid of cell center points
        xfce (punch.grid.Grid): a grid of x-face points
        yfce (punch.grid.Grid): a grid of y-face points
        zfce (punch.grid.Grid): a grid of z-face points
        deltax (float): grid spacing in the x-direction
        deltay (float): grid spacing in the y-direction
        deltaz (float): grid spacing in the z-direction
        lengthx (float): domain length in the x-direction
        lengthy (float): domain length in the y-direction
        lengthz (float): domain length in the z-direction
    """

    def __init__(self,
                 ncells,
                 boxdims=[[0., 1.], [0., 1.], [0., 1.]],
                 ghost_cells=False):
        """
        Make an FVGrid
        :param ncells (list (int)*3): the number of cells in the x-, y-, and z-directions
        :param boxdims (list (float, float)*3): domain bounds in the x-, y-, and z-directions
        :param ghost_cells (bool): whether or not to include ghost cells, default False
        """

        if ghost_cells:
            deltas = [(bx[1] - bx[0]) / float(nc) for (bx, nc) in zip(boxdims, ncells)]

            ncells = [ncell + 2 for ncell in ncells]
            boxdims = [[bx[0] - dx, bx[1] + dx] for (bx, dx) in zip(boxdims, deltas)]

        npts_crnrs = [n + 1 for n in ncells]
        npts_cells = ncells
        npts_xfces = [ncells[0] + 1, ncells[1], ncells[2]]
        npts_yfces = [ncells[0], ncells[1] + 1, ncells[2]]
        npts_zfces = [ncells[0], ncells[1], ncells[2] + 1]

        if not ghost_cells:
            deltas = [(bx[1] - bx[0]) / float(nc) for (bx, nc) in zip(boxdims, ncells)]

        dims_edges = boxdims
        dims_cells = [[bx[0] + 0.5 * delta, bx[1] - 0.5 * delta] for (bx, delta) in zip(boxdims, deltas)]
        dims_xfces = [[boxdims[0][0], boxdims[0][1]],
                      [boxdims[1][0] + 0.5 * deltas[1], boxdims[1][1] - 0.5 * deltas[1]],
                      [boxdims[2][0] + 0.5 * deltas[2], boxdims[2][1] - 0.5 * deltas[2]]]
        dims_yfces = [[boxdims[0][0] + 0.5 * deltas[0], boxdims[0][1] - 0.5 * deltas[0]],
                      [boxdims[1][0], boxdims[1][1]],
                      [boxdims[2][0] + 0.5 * deltas[2], boxdims[2][1] - 0.5 * deltas[2]]]
        dims_zfces = [[boxdims[0][0] + 0.5 * deltas[0], boxdims[0][1] - 0.5 * deltas[0]],
                      [boxdims[1][0] + 0.5 * deltas[1], boxdims[1][1] - 0.5 * deltas[1]],
                      [boxdims[2][0], boxdims[2][1]]]

        self.crnr = Grid(npts_crnrs, dims_edges)
        self.cell = Grid(npts_cells, dims_cells)
        self.xfce = Grid(npts_xfces, dims_xfces)
        self.yfce = Grid(npts_yfces, dims_yfces)
        self.zfce = Grid(npts_zfces, dims_zfces)
        self.deltax = deltas[0]
        self.deltay = deltas[1]
        self.deltaz = deltas[2]
        self.lengthx = self.crnr.x[-1] - self.crnr.x[0]
        self.lengthy = self.crnr.y[-1] - self.crnr.y[0]
        self.lengthz = self.crnr.z[-1] - self.crnr.z[0]

    def write_matlab(self, folderpath):
        """
        Write the grids associated with this FVGrid to matlab binary in a directory
        :param folderpath (str): the directory where grid .mat files will be written
        """
        if not os.path.exists(folderpath):
            os.makedirs(folderpath)
        self.crnr.write_matlab(folderpath + '/corner-points')
        self.cell.write_matlab(folderpath + '/cell-points')
        self.xfce.write_matlab(folderpath + '/xface-points')
        self.yfce.write_matlab(folderpath + '/yface-points')
        self.zfce.write_matlab(folderpath + '/zface-points')
        scio.savemat(folderpath + '/gridspecs.mat', {'deltax': self.deltax,
                                                     'deltay': self.deltay,
                                                     'deltaz': self.deltaz,
                                                     'lengthx': self.lengthx,
                                                     'lengthy': self.lengthy,
                                                     'lengthz': self.lengthz})


def plot_domain_face_area(boxdims,
                          axis,
                          face,
                          edgecolor='b',
                          facecolor='b',
                          fillalpha=0.2):
    """
    Plot a box representing a face of a domain, with matplotlib.
    :param boxdims (list (float, float)*3): domain bounds in the x-, y-, and z-directions
    :param axis: a matplotlib axis object onto which the box is drawn
    :param face (str): the face of interest, options are 'xy', 'xz', and 'yz'
    :param edgecolor: color of the box edge, default 'b'
    :param facecolor: color of the box face, default 'b'
    :param fillalpha (float): transparency of the box face, default 0.2
    """
    if face == 'xy':
        boxspecs = [(boxdims[0][0], boxdims[1][0]),
                    boxdims[0][1] - boxdims[0][0],
                    boxdims[1][1] - boxdims[1][0]]
    elif face == 'xz':
        boxspecs = [(boxdims[0][0], boxdims[2][0]),
                    boxdims[0][1] - boxdims[0][0],
                    boxdims[2][1] - boxdims[2][0]]
    elif face == 'yz':
        boxspecs = [(boxdims[1][0], boxdims[2][0]),
                    boxdims[1][1] - boxdims[1][0],
                    boxdims[2][1] - boxdims[2][0]]

    axis.add_patch(patches.Rectangle(*boxspecs,
                                     alpha=fillalpha,
                                     facecolor=facecolor,
                                     edgecolor=facecolor))
    axis.add_patch(patches.Rectangle(*boxspecs,
                                     fill=False,
                                     edgecolor=edgecolor))


def plot_cell_face_area(fvgrid,
                        axis,
                        face):
    """
    Plot a box around the finite volume cell in the (1, 1) place on the specified face.
    For demonstration purposes.
    :param fvgrid (punch.grid.FVGrid): a FVGrid object
    :param axis: a matplotlib axis object onto which the box is drawn
    :param face: the face of interest, options are 'xy', 'xz', and 'yz'
    """
    if face == 'xy':
        d1 = fvgrid.crnr.XYx
        d2 = fvgrid.crnr.XYy
        delta1 = fvgrid.deltax
        delta2 = fvgrid.deltay
    elif face == 'xz':
        d1 = fvgrid.crnr.XZx
        d2 = fvgrid.crnr.XZz
        delta1 = fvgrid.deltax
        delta2 = fvgrid.deltaz
    elif face == 'yz':
        d1 = fvgrid.crnr.YZy
        d2 = fvgrid.crnr.YZz
        delta1 = fvgrid.deltay
        delta2 = fvgrid.deltaz

    axis.add_patch(patches.Rectangle((d1[1, 1], d2[1, 1]),
                                     delta1,
                                     delta2,
                                     facecolor='r',
                                     alpha=0.4,
                                     edgecolor='r'))
    axis.add_patch(patches.Rectangle((d1[1, 1], d2[1, 1]),
                                     delta1,
                                     delta2,
                                     fill=False,
                                     edgecolor='r'))


def plot_labeled_points(fvgrid,
                        axis,
                        face):
    """
    Plot points on a domain face as cell centers (square), faces (x, y, z), and corners (dot)
    For demonstration purposes
    :param fvgrid (punch.grid.FVGrid): a FVGrid object
    :param axis: a matplotlib axis object onto which the markers are drawn
    :param face: the face of interest, options are 'xy', 'xz', and 'yz'
    """
    if face == 'xy':
        axis.plot(fvgrid.cell.XYx, fvgrid.cell.XYy, 'k', marker='s', linestyle='')
        axis.plot(fvgrid.crnr.XYx, fvgrid.crnr.XYy, 'k', marker='.', linestyle='')
        axis.plot(fvgrid.xfce.XYx, fvgrid.xfce.XYy, 'k', marker='$x$', linestyle='')
        axis.plot(fvgrid.yfce.XYx, fvgrid.yfce.XYy, 'k', marker='$y$', linestyle='')
    elif face == 'xz':
        axis.plot(fvgrid.cell.XZx, fvgrid.cell.XZz, 'k', marker='o', linestyle='')
        axis.plot(fvgrid.crnr.XZx, fvgrid.crnr.XZz, 'k', marker='.', linestyle='')
        axis.plot(fvgrid.xfce.XZx, fvgrid.xfce.XZz, 'k', marker='$x$', linestyle='')
        axis.plot(fvgrid.zfce.XZx, fvgrid.zfce.XZz, 'k', marker='$z$', linestyle='')
    elif face == 'yz':
        axis.plot(fvgrid.cell.YZy, fvgrid.cell.YZz, 'k', marker='o', linestyle='')
        axis.plot(fvgrid.crnr.YZy, fvgrid.crnr.YZz, 'k', marker='.', linestyle='')
        axis.plot(fvgrid.yfce.YZy, fvgrid.yfce.YZz, 'k', marker='$y$', linestyle='')
        axis.plot(fvgrid.zfce.YZy, fvgrid.zfce.YZz, 'k', marker='$z$', linestyle='')


def demo_grid(face):
    """
    Demonstrate a simple finite volume grid
    :param face: the face of interest, options are 'xy', 'xz', and 'yz'
    """
    ncells = [5, 4, 3]  # x, y, z
    boxdims = [[0.25, 1.],  # x
               [0., 0.90],  # y
               [0.1, 0.8]]  # z

    m = FVGrid(ncells, boxdims)

    plot_domain_face_area(boxdims, plt.gca(), face)
    plot_cell_face_area(m, plt.gca(), face)
    plot_labeled_points(m, plt.gca(), face)
    plt.title(face + ' face')

    plt.xlim([-0.1, 1.1])
    plt.ylim([-0.1, 1.1])
    plt.xlabel(face[0])
    plt.ylabel(face[1])
    plt.show()


def test_grid():
    """
    Runs some simple tests of the basic punch.Grid class
    :return: a punch.Test object, call print on it to see the overall test result
    """
    npts = [5, 5, 6]
    dims = [[-0.5, +0.5], [-0.25, +0.75], [0.0, +1.0]]
    g = Grid(npts, dims)
    gt = pct.Test('punch.Grid')
    gt(pct.compare_float_arrays(g.x, np.array([-0.5, -0.25, 0.0, +0.25, +0.5])), 'x points')
    gt(pct.compare_float_arrays(g.y, np.array([-0.25, 0.0, +0.25, +0.5, +0.75])), 'y points')
    gt(pct.compare_float_arrays(g.z, np.array([0.0, +0.2, +0.4, +0.6, +0.8, +1.0])), 'z points')
    XYxt, XYyt = np.meshgrid(g.x, g.y, indexing='ij')
    XZxt, XZzt = np.meshgrid(g.x, g.z, indexing='ij')
    YZyt, YZzt = np.meshgrid(g.y, g.z, indexing='ij')
    gt(pct.compare_float_arrays(g.XYx, XYxt), 'xy x points')
    gt(pct.compare_float_arrays(g.XYy, XYyt), 'xy y points')
    gt(pct.compare_float_arrays(g.XZx, XZxt), 'xz x points')
    gt(pct.compare_float_arrays(g.XZz, XZzt), 'xz z points')
    gt(pct.compare_float_arrays(g.YZy, YZyt), 'yz y points')
    gt(pct.compare_float_arrays(g.YZz, YZzt), 'yz z points')
    return gt


def test_fvgrid():
    """
    Runs some simple tests of the punch.FVGrid class
    :return: a punch.Test object, call print on it to see the overall test result
    """
    ncells = [5, 5, 6]
    boxdims = [[-0.5, +0.5], [-0.25, +0.75], [0.0, +1.0]]
    xspacing = 0.2
    yspacing = 0.2
    zspacing = 1.0 / 6.0
    g = FVGrid(ncells, boxdims)
    gt = pct.Test('punch.FVGrid')
    gt(pct.compare_floats(g.deltax, xspacing), 'x spacing')
    gt(pct.compare_floats(g.deltay, yspacing), 'y spacing')
    gt(pct.compare_floats(g.deltaz, zspacing), 'z spacing')
    gt(pct.compare_float_arrays(g.cell.x[0], (-0.4)), 'cell x- endpoint')
    gt(pct.compare_float_arrays(g.cell.x[-1], (+0.4)), 'cell x+ endpoint')
    gt(pct.compare_float_arrays(g.cell.y[0], (-0.15)), 'cell y- endpoint')
    gt(pct.compare_float_arrays(g.cell.y[-1], (+0.65)), 'cell y+ endpoint')
    gt(pct.compare_float_arrays(g.cell.z[0], (0.5 / 6.0)), 'cell z- endpoint')
    gt(pct.compare_float_arrays(g.cell.z[-1], (5.5 / 6.0)), 'cell z+ endpoint')
    gt(pct.compare_float_arrays(g.xfce.x[0], (-0.5)), 'xface x- endpoint')
    gt(pct.compare_float_arrays(g.xfce.x[-1], (+0.5)), 'xface x+ endpoint')
    gt(pct.compare_float_arrays(g.xfce.y[0], (-0.15)), 'xface y- endpoint')
    gt(pct.compare_float_arrays(g.xfce.y[-1], (+0.65)), 'xface y+ endpoint')
    gt(pct.compare_float_arrays(g.xfce.z[0], (0.5 / 6.0)), 'xface z- endpoint')
    gt(pct.compare_float_arrays(g.xfce.z[-1], (5.5 / 6.0)), 'xface z+ endpoint')
    gt(pct.compare_float_arrays(g.yfce.x[0], (-0.4)), 'yface x- endpoint')
    gt(pct.compare_float_arrays(g.yfce.x[-1], (+0.4)), 'yface x+ endpoint')
    gt(pct.compare_float_arrays(g.yfce.y[0], (-0.25)), 'yface y- endpoint')
    gt(pct.compare_float_arrays(g.yfce.y[-1], (+0.75)), 'yface y+ endpoint')
    gt(pct.compare_float_arrays(g.yfce.z[0], (0.5 / 6.0)), 'yface z- endpoint')
    gt(pct.compare_float_arrays(g.yfce.z[-1], (5.5 / 6.0)), 'yface z+ endpoint')
    gt(pct.compare_float_arrays(g.zfce.x[0], (-0.4)), 'zface x- endpoint')
    gt(pct.compare_float_arrays(g.zfce.x[-1], (+0.4)), 'zface x+ endpoint')
    gt(pct.compare_float_arrays(g.zfce.y[0], (-0.15)), 'zface y- endpoint')
    gt(pct.compare_float_arrays(g.zfce.y[-1], (+0.65)), 'zface y+ endpoint')
    gt(pct.compare_float_arrays(g.zfce.z[0], (0.0)), 'zface z- endpoint')
    gt(pct.compare_float_arrays(g.zfce.z[-1], (+1.0)), 'zface z+ endpoint')
    gt(pct.compare_float_arrays(g.crnr.x[0], (-0.5)), 'corner x- endpoint')
    gt(pct.compare_float_arrays(g.crnr.x[-1], (+0.5)), 'corner x+ endpoint')
    gt(pct.compare_float_arrays(g.crnr.y[0], (-0.25)), 'corner y- endpoint')
    gt(pct.compare_float_arrays(g.crnr.y[-1], (+0.75)), 'corner y+ endpoint')
    gt(pct.compare_float_arrays(g.crnr.z[0], (0.0)), 'corner z- endpoint')
    gt(pct.compare_float_arrays(g.crnr.z[-1], (+1.0)), 'corner z+ endpoint')
    return gt


if __name__ == '__main__':
    gridtests = []
    gridtests.append(test_grid())
    gridtests.append(test_fvgrid())

    print('')
    for gt in gridtests:
        print(gt)
